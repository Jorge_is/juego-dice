///////////////////////////////////////
// Lecture: Hoisting
calculateAge(1990);
function  calculateAge(year){
    console.log(2016-year);
}


var retiro = function(year){
    console.log(65-(2016-year));

}
retiro(1990);

 var numeros = [1,2,3,4];
numeros.push(6);
console.log(numeros);
 
var ter = [];
for (let i = 0; i <= 10; i++) {
    
    var lit = ter.push(i);
    else if (lit >=12){
        console.log('No hay suficientes manzanas');
    }
}
 console.log(ter);










///////////////////////////////////////
// Lecture: Scoping


// First scoping example

/*
var a = 'Hello!';
first();

function first() {
    var b = 'Hi!';
    second();

    function second() {
        var c = 'Hey!';
        console.log(a + b + c);
    }
}
*/



// Example to show the differece between execution stack and scope chain

/*
var a = 'Hello!';
first();

function first() {
    var b = 'Hi!';
    second();

    function second() {
        var c = 'Hey!';
        third()
    }
}

function third() {
    var d = 'John';
    console.log(a + b + c + d);
}
*/



///////////////////////////////////////
// Lecture: The this keyword









